import { Route, Link, Routes } from "react-router-dom";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Home from "./pages/home";
import Create from "./pages/create";
import Update from "./pages/update";
function App() {
  return (
    <div>
      
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        {/* <Link className="navbar-brand" href="#">Navbar</Link> */}
        <button className="navbar-toggler"type="button"data-toggle="collapse"data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"aria-expanded="false"aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="container-fluid collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <Link className="nav-link" to={"/"}>View Table</Link>
            </li>
            <li className="nav-item active">
              <Link className="nav-link" to={"/create"}>Create Table</Link>
            </li>
          </ul>
        </div>
      </nav>
      <div className="container">
        <Routes>
          <Route path="/" element={<Home />}></Route>
          <Route path="/update/:id" element={<Update />}></Route>
          <Route path="/create" element={<Create />}></Route>
        </Routes>
      </div>
    </div>
  );
}

export default App;
