import axios from "axios";
import React, { useState, useEffect, Navigate } from "react";
// import { Link, useParams } from "react-router-dom";
import { Link, useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
export default function Update() {
  let { id } = useParams();
  const navigate = useNavigate();
  const [InputErrorList, setInputErrorList] = useState({});
  const [user, setUsers] = useState({
    name: "",
    lname: "",
    email: "",
    address: "",
    dob: "",
    phone: "",
  });

  useEffect(() => {
    // alert(id);
    axios.get("http://127.0.0.1:8000/api/edit/" + id).then((res) => {
      let updateUsers = { ...user };
      updateUsers["name"] = res.data.data.name;
      updateUsers["lname"] = res.data.data.lname;
      updateUsers["email"] = res.data.data.email;
      updateUsers["address"] = res.data.data.address;
      updateUsers["dob"] = res.data.data.dob;
      updateUsers["phone"] = res.data.data.phone;
      setUsers(updateUsers);
    });
  }, []);

  const handleInput = (e) => {
    setUsers({ ...user, [e.target.name]: e.target.value });
  };
  const saveusers = (e) => {
    e.preventDefault();

    axios
      .post("http://127.0.0.1:8000/api/update/" + id, user)
      .then((res) => {
        // toast.success("User is update");
        toast.success("User Has Been Updated", {
          position: "top-right",
          autoClose: 2500,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "dark",
        });
        navigate("/");
      })
      .catch(function (error) {
        if (error.response) {
          if (error.response.status === 422) {
            setInputErrorList(error.response.data.errors);
          }
          if (error.response.status === 200) {
            alert(error.response.data);
          }
        }
      });
  };
  return (
    <div className="container mt-4">
      <div className="row">
        <div className="col-md-12">
          <div className="card">
          <div className="card-header">
              <h3>
                Update User Form
              </h3>
            </div>
            <div className="card-body">
              <form onSubmit={saveusers}>
                <div class="row mt-3">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="">Name</label>
                      <input
                        type="text"
                        class="form-control"
                        name="name"
                        value={user.name}
                        onChange={handleInput}
                        aria-describedby="helpId"
                        placeholder="Enter Your Name"
                      />
                      <span className="text-danger">{InputErrorList.name}</span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="">Last Name</label>
                      <input
                        type="text"
                        class="form-control"
                        name="fname"
                        value={user.lname}
                        onChange={handleInput}
                        aria-describedby="helpId"
                        placeholder="Enter Your Father Name"
                      />
                      <span className="text-danger">
                        {InputErrorList.lname}
                      </span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group mt-4">
                      <label for="">Email</label>
                      <input
                        type="email"
                        class="form-control"
                        name="email"
                        value={user.email}
                        onChange={handleInput}
                        aria-describedby="helpId"
                        placeholder="Enter Your Email"
                      />
                      <span className="text-danger">
                        {InputErrorList.email}
                      </span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group mt-4">
                      <label for="">Address</label>
                      <input
                        type="text"
                        class="form-control"
                        name="address"
                        value={user.address}
                        onChange={handleInput}
                        aria-describedby="helpId"
                        placeholder="Enter Your Address"
                      />
                      <span className="text-danger">
                        {InputErrorList.address}
                      </span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group mt-4">
                      <label for="">Date Of Birth</label>
                      <input
                        type="date"
                        class="form-control"
                        name="dob"
                        value={user.dob}
                        onChange={handleInput}
                        aria-describedby="helpId"
                        placeholder=""
                      />
                      <span className="text-danger">{InputErrorList.dob}</span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group mt-4">
                      <label for="">Phone Number</label>
                      <input
                        type="number"
                        class="form-control"
                        name="phone"
                        value={user.phone}
                        onChange={handleInput}
                        aria-describedby="helpId"
                        placeholder="Enter Your Number"
                      />
                      <span className="text-danger">
                        {InputErrorList.phone}
                      </span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 mt-4">
                    <button class="btn btn-primary" id="btn" type="submit">
                      Update
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
// export default userCreate;
