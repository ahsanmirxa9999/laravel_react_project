import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { toast } from "react-toastify";
// import http from '../http'
function Home() {
  const [users, setUsers] = useState([]);
  useEffect(() => {
    getUser();
  }, []);

  const getUser=()=>{
    axios.get("http://127.0.0.1:8000/api/getuser").then((res) => {
      console.log(res);
      setUsers(res.data.data);
    });
  }
  const deletefun = (e, id) => {
    e.preventDefault();
    axios.get(`http://127.0.0.1:8000/api/delete/`+ id).then(res => {
    getUser();

        // toast.error('Student Has Been Deleted!');
        toast.error("User Has Been Deleted!", {
          position: "top-right",
          autoClose: 2500,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "dark",
        });
    })
    .catch(function(error){
      if(error.response){
        if(error.response.status === 422){
          alert(error.response.delete.delete)
        }
        if(error.response.status === 200){
          alert(error.response.data)
        }
      }
    });
  }
  
  const userDetails =
    users &&
    users.map((item, index) => {
      return (
        <tr key={index}>
          <td>{index + 1}</td>
          {/* <td>{item.id}</td> */}
          <td>{item.name}</td>
          <td>{item.lname}</td>
          <td>{item.email}</td>
          <td>{item.phone}</td>
          <td>{item.address}</td>
          <td>{item.dob}</td>
          <td>
            <Link className="btn btn-success" to={`/update/${item.id}`}>
              Update
            </Link>
          </td>
          <td>
            <button
              className="btn btn-danger"
              onClick={(e) => deletefun(e, item.id)}>
              Delete
            </button>
          </td>
        </tr>
      );
    });

  return (
    <div className="container mt-4">
      <div className="row">
        <div className="col-md-12">
          <div className="card">
            <div className="card-header">
              <h3>
                Users List{" "}
                <Link
                  to="/create"
                  className="btn btn-primary float-end"
                >
                  Add Student
                </Link>{" "}
              </h3>
            </div>
            <div className="card-body">
              <table className="table table-striped">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>Date of Birth</th>
                    <th>Update</th>
                    <th>Delete</th>
                  </tr>
                </thead>
                <tbody>{userDetails}</tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;
