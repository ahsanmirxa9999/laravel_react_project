import axios from "axios";
import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
function Create() {
  const navigate = useNavigate();
  const [InputErrorList, setInputErrorList] = useState({});
  const [user, setUser] = useState({
    name: "",
    lname: "",
    email: "",
    address: "",
    dob: "",
    phone: "",
  });
  const handleInput = (e) => {
    // e.persist();
    setUser({ ...user, [e.target.name]: e.target.value });
  };
  const saveuser = (e) => {
    e.preventDefault();
    axios
      .post("http://127.0.0.1:8000/api/createuser", user)
      .then((res) => {
        alert("User has been inserted");
        // toast.success("user Has Been Inserted");
        toast.success('User Has Been Inserted', {
          position: "top-right",
          autoClose: 2500,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "dark",
          });
        navigate('/');
      })
      .catch(function (error) {
        if (error.response) {
          if (error.response.status === 422) {
            setInputErrorList(error.response.data.errors);
          }
          if (error.response.status === 200) {
            alert(error.response.data);
          }
        }
      });
  };

  return (
    <div className="container mt-4">
      <div className="row">
        <div className="col-md-12">
          <div className="card">
            <div className="card-header">
              <h3>
                {" "}
                Add User Form{" "}
                <Link to="/" className="btn btn-primary float-end">
                  View user
                </Link>
              </h3>
            </div>
            <div className="card-body">
              <form onSubmit={saveuser}>
                <div className="row mt-3">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="">Name</label>
                      <input
                        type="text"
                        className="form-control"
                        name="name"
                        value={user.name}
                        onChange={handleInput}
                        aria-describedby="helpId"
                        placeholder="Enter Your Name"
                      />
                      <span className="text-danger">{InputErrorList.name}</span>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="">Last Name</label>
                      <input
                        type="text"
                        className="form-control"
                        name="lname"
                        value={user.lname}
                        onChange={handleInput}
                        aria-describedby="helpId"
                        placeholder="Enter Your Father Name"
                      />
                      <span className="text-danger">
                        {InputErrorList.fname}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group mt-4">
                      <label htmlFor="">Email</label>
                      <input
                        type="email"
                        className="form-control"
                        name="email"
                        value={user.email}
                        onChange={handleInput}
                        aria-describedby="helpId"
                        placeholder="Enter Your Email"
                      />
                      <span className="text-danger">
                        {InputErrorList.email}
                      </span>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group mt-4">
                      <label htmlFor="">Address</label>
                      <input
                        type="text"
                        className="form-control"
                        name="address"
                        value={user.address}
                        onChange={handleInput}
                        aria-describedby="helpId"
                        placeholder="Enter Your Address"
                      />
                      <span className="text-danger">
                        {InputErrorList.address}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group mt-4">
                      <label htmlFor="">Date Of Birth</label>
                      <input
                        type="date"
                        className="form-control"
                        name="dob"
                        value={user.dob}
                        onChange={handleInput}
                        aria-describedby="helpId"
                        placeholder=""
                      />
                      <span className="text-danger">{InputErrorList.dob}</span>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group mt-4">
                      <label htmlFor="">Phone Number</label>
                      <input
                        type="number"
                        className="form-control"
                        name="phone"
                        value={user.phone}
                        onChange={handleInput}
                        aria-describedby="helpId"
                        placeholder="Enter Your Number"
                      />
                      <span className="text-danger">
                        {InputErrorList.phone}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12 mt-4">
                    <button className="btn btn-primary" id="btn" role="button">
                      Submit
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Create;
