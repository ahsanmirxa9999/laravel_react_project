<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::resource('user',UserController::class);

Route::get('/getuser',[UserController::class,'show']);
Route::post('/createuser',[UserController::class,'create']);
Route::get('/edit/{id}',[UserController::class,'edit']);
Route::get('/delete/{id}',[UserController::class,'delete']);
Route::post('/update/{id}',[UserController::class,'update']);
