<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{

    public function create(Request $request)
    {
        $create = User :: create([
            'name'=>$request->name,
            'lname'=>$request->lname,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'address'=>$request->address,
            'dob'=>$request->dob,
        ]);
        if($create){
            dd('Student Has Been Inserted');
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function show(Request $request)
    {
        $data = User::all();
        return response()->json([
            'status' => 200,
            'data' => $data
        ]);
    }

    public function delete($id)
    {
        $delete = User::where('id', $id)->delete();
        if ($delete) {
            return response()->json([
                'status' => 200,
                'delete' => 'Student Has Been Deleted'
            ]);
        }
    }

    public function edit($id)
    {
        $data = User::where('id', $id)->first();
        return response()->json([
            'status' => 200,
            'data' => $data
        ]);
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if(!$user){
            return response()->json(['error'=>'Student is not found'],404);
        }
        $user->name = $request->input('name');
        $user->lname = $request->input('lname');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $user->address = $request->input('address');
        $user->dob = $request->input('dob');
        $user->save();
        return response()->json([ 'message' => 'Student Has Been Updated' ]);
    }


}
